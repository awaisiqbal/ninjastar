# README #

### What is this repository for? ###

* This project is maked to create a new Game for Android Phone, currently is only available for Eclipse terminal.
* Version 0.1


### Requiriments ###

* Eclipse
* JRE1.8.0

### How do I get set up? ###
    
* Use git clone : 
```
#!bash
git clone git@bitbucket.org:awaisiqbal/ninjastar.git
```
* Open Eclipse 
* Choose: File > Import... > "Existing Projects into Workspace" 
* Select the directory where you have cloned the project
* Select the 2 Projects: NinjaStar-server & NinjaStar-client
* Press Finish.
* Now you have the project ready to use

In case that you have any problem with Java Problem:
you can go to Window > preferences > Java > Installed JREs and add the jre you want to use


### How to Run the program ###


1. Select the project NinjaStar-server in the Package Explorer and press Run
2. Select the NinjaStar-client in the Package Explorer and press Run
3. Do the same says in the step 2 3 times more to start game because the game is only avaible for 4 players

### Contribution guidelines ###

* There is many lines in the project with //TODO to complete

### Who do I talk to? ###

* Awais Iqbal Begum (awais.iqbal2@gmail.com)