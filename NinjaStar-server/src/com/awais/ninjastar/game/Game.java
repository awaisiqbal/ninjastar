/* Game.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.game;

import java.util.ArrayList;

import com.awais.ninjastar.communication.Table;
import com.awais.ninjastar.hand.ActionCard;
import com.awais.ninjastar.hand.Card;
import com.awais.ninjastar.hand.PropertyCard;
import com.awais.ninjastar.hand.WeaponCard;
import com.awais.ninjastar.message.CardUsageMessage;
import com.awais.ninjastar.message.Message;
import com.awais.ninjastar.message.MessageFactory;
import com.awais.ninjastar.message.OptionsMessage;
import com.awais.ninjastar.message.PlayerUpdateMessage;
import com.awais.ninjastar.player.Player;
import com.awais.ninjastar.player.Roles;

public class Game {
	/**
	 * This game's croupier
	 */
	private Croupier croupier;

	/**
	 * Array with all the players of this game at their respective positions.
	 */
	private ArrayList<Player> players;

	/**
	 * Player holding the current turn
	 */
	private Player playerCurrentTurn;

	/**
	 * Table where the game is playing
	 */
	private Table table;

	/**
	 * Shogun seating position
	 */
	private int shogunID;

	/**
	 * This attribute represents the options pending to select
	 */
	private int optionsPending = 0;

	/**
	 * Boolean that indicates if the game is in TEST mode or not
	 */
	public final boolean TEST = false;

	/**
	 * Currently the max capacity of the game is for 4 players
	 */
	public final int MAX_CAPACITY = 4;

	public Game(Table t) {
		this.croupier = new Croupier(this);
		this.players = new ArrayList<>();
		this.table = t;
	}

	/**
	 * Gets clients' actions from ClientThread, modifies the game accordingly,
	 * and sends a message back to the client.
	 * 
	 * @param msg
	 *            , message from the client
	 * @param pid
	 *            , player ID
	 * @return Message to be sent back to client
	 */
	public Message consumeMessage(Message msg, int pid) {
		System.out.println("Entering in ConsumeMessage");
		Message msgToRet = null;
		Player p = getPlayers().get(pid);
		switch (msg.getCode()) {
		case MessageFactory.MSG_NULL:
			System.out.println("Null message received");
			break;
		case MessageFactory.MSG_TURNEND:
			System.out.println(p.getSeat() + " ended hes turn!");
			nextTurn();
			break;
		case MessageFactory.MSG_DISCONNECT:
			msgToRet = MessageFactory.createDisconnectMessage();
			System.out.println("Someone left the game... Shutting down...");
			onGameEnd();
			// removePlayer(pid);
			break;
		case MessageFactory.MSG_OPTIONS:
			System.out.println("Someone used a option");
			if (optionsPending > 0) {
				OptionsMessage om = (OptionsMessage) msg;
				manageOptions(pid, om.getOptionsID(), om.getSelectedOption());
				optionsPending--;
			}
			break;
		case MessageFactory.MSG_CARDUSAGE:
			CardUsageMessage cum = (CardUsageMessage) msg;
			System.out.println("Card useed: player: " + cum.getObjectivePID() + "\tCard: " + cum.getUsedCardID());
			if (optionsPending == 0) {
				useCard(cum);
				startTurnAgain();
			} else {
				table.sendMessageToSeat(pid, MessageFactory.createErrorMessage("Options to select left!"));
			}
			break;
		default:
			break;
		}
		if (msgToRet != null) {
			msgToRet.setPlayerId(pid);
		}
		return msgToRet; // msgToRet message to return to the client who send us
							// the message
	}

	private void manageOptions(int pid, int optionsID, int selectedOption) {
		Player p = players.get(pid);
		switch (optionsID) {
		case OptionsMessage.ATTACK: // future do a option on Attack

			break;
		case OptionsMessage.BREATHING: // Breathing
			Player p2 = players.get(selectedOption);
			p2.addCard(croupier.getOneCard());
			break;
		case OptionsMessage.GEISHA: // Geisha
			Card c = Card.getByID(selectedOption);
			removeCardFromGameAndPutInWaste(c);
			break;
		case OptionsMessage.JIUJITSU: // Jiu-Jitsu

			if (selectedOption == 0) { // default loose 1 resistence
				reduceResistance(p);
			} else {
				for (int i = 0; i < p.getHand().size(); i++) {
					Card posibleWeaponCard = p.getHand().get(i);
					if (posibleWeaponCard instanceof WeaponCard) { // TODO In
																	// future
																	// send
						// option select to
						// weapon to delete
						p.removeCard(posibleWeaponCard);
					}
				}

			}
			break;
		case OptionsMessage.BATTLECRY: // Battle Cry
			if (selectedOption == 0) {
				reduceResistance(p);
			} else {
				// TODO check if the player has a stop Card
			}
			break;
		case OptionsMessage.BUSHIDO: // used by card Bushido
			if (selectedOption == 0) {

			} else {

			}
			break;
		case 6: // Not used

			break;

		}

	}

	private void reduceResistance(Player p) {
		p.loseResistence();
		if (p.getHonor() == 0)
			onGameEnd();
	}

	private void removeCardFromGameAndPutInWaste(Card c) {
		// TODO Pendiente de hacer quitar una carta de la mesa y ponerla en
		// descarte

	}

	private void startTurnAgain() {
		table.sendNextTurnMsg();
	}

	/**
	 * This method does all the actions related to that card
	 * 
	 * @param cardID
	 *            ID of the card used
	 * @param ownSeat
	 *            Player who use the card
	 * @param objectiveSeat
	 *            Seat of the objective player, if the card dosen't have a
	 *            objective should be -1
	 * @param onlyCheck
	 *            True if only want to know if is this action possible
	 */
	private boolean useCard(CardUsageMessage cum) {
		int cardID = cum.getUsedCardID();
		int ownSeat = cum.getPlayerId();
		int objectiveSeat = cum.getObjectivePID();
		if (!players.get(ownSeat).contains(cardID)) {
			System.err.println("Player don't have that card");
			Message rtn = MessageFactory.createErrorMessage("You don't have the card u used");
			table.sendMessageToSeat(ownSeat, rtn);
			return false;
		}
		Card c = Card.getByID(cardID);
		boolean done = false; // variable used to know if the card is used or
								// not

		Player pOwn = players.get(ownSeat);
		if (cardID >= 0 && cardID <= 31) {// it's a Weapon
			System.out.println("Using a card");
			WeaponCard wc = (WeaponCard) c;
			int distancia = calculateDistance(ownSeat, objectiveSeat);
			boolean distanceOk = distancia <= wc.getScope();
			System.err.println("Distance: " + distancia + "\t" + wc.getScope());
			Message rtn;
			Player pObj = players.get(objectiveSeat);
			if (pObj.getCurrentAtacksUseds() < pObj.getPossibleAtacks()) {
				if (distanceOk) {
					pObj.receiveDamage(wc.getDamage() + pOwn.getExtraDamage());
					rtn = createUpdateMessage(pObj);
					table.sendMessageBroadcast(rtn);
					done = true;
				} else {
					System.err.println("Distance no OK");
					rtn = MessageFactory.createErrorMessage("That player is too far, weaponDistance: " + wc.getScope()
							+ " And the objective is at: " + distancia);
					table.sendMessageToSeat(ownSeat, rtn);
				}
			} else {
				rtn = MessageFactory.createErrorMessage("You already used all the posible attacks");
				table.sendMessageToSeat(ownSeat, rtn);
			}

		} else if (cardID >= 32 && cardID <= 46) {// it's a Property
			if (cardID == 38 && cardID == 39) {
				// TODO check case of bushido

				// use removeCardFromPlayerBenefits
			} else {
				PropertyCard pc = (PropertyCard) c;
				if (cardID >= 32 && cardID <= 37) {
					pOwn.setPossibleAtacks(pOwn.getPossibleAtacks() + 1);
				} else if (cardID >= 40 && cardID <= 42) {
					pOwn.setExtraDamage(pOwn.getExtraDamage() + 1);
				} else if (cardID >= 43 && cardID <= 46) {
					pOwn.setExtraDificulty(pOwn.getExtraDificulty() + 1);
				}
			}
			pOwn.addBenefitCard(c);
		} else if (cardID >= 47 && cardID <= 89) { // it's a Action
			done = useAction(pOwn, cardID, objectiveSeat);
		}

		if (done) {
			players.get(ownSeat).removeCard(c);// remove that card
												// from the users
												// hand

			croupier.putWaste(c); // add the used card in the waste
			// stack
			table.sendMessageBroadcast(createUpdateMessage(pOwn));
		}
		return done;

	}

	private void removeCardFromPlayerBenefits(Player player, Card c) {
		int cardID = c.getCardID();

		if (cardID >= 33 && cardID <= 37) { // remove Concentration
			player.setPossibleAtacks(player.getPossibleAtacks() - 1);
		} else if (cardID >= 40 && cardID <= 42) { // remove Fast unsheathed
			player.setExtraDamage(player.getExtraDamage() + 1);
		} else if (cardID >= 43 && cardID <= 46) { // remove Armor
			player.setExtraDificulty(player.getExtraDificulty() + 1);
		}
		player.removeCard(c);

	}

	private boolean useAction(Player pOwn, int cardID, int objectiveSeat) {
		boolean done = true;
		if (cardID >= 47 && cardID <= 51) { // distraction
			pOwn.addCard(croupier.getOneCard());
		} else if (cardID >= 52 && cardID <= 54) {// Daimio
			pOwn.addCard(croupier.getOneCard());
			pOwn.addCard(croupier.getOneCard());
		} else if (cardID >= 55 && cardID <= 57) {// Breathing
			pOwn.restoreResistance();
			Player p = players.get(objectiveSeat);
			p.addCard(croupier.getOneCard());
			table.sendMessageBroadcast(createUpdateMessage(p));
		} else if (cardID >= 58 && cardID <= 63) {// Geisha
			findAndputCardInWaste(cardID);
		} else if (cardID >= 64 && cardID <= 66) {// Jiu-jitsu
			ArrayList<String> al = new ArrayList<>();
			al.add("1. Lose resistance");
			al.add("2. Lose Weapon");
			OptionsMessage om = MessageFactory.createOptionsMessageJiuJitsu(al);
			BroadcastOptions(om);
			done = true;
		} else if (cardID >= 67 && cardID <= 70) {// Battle Cry
			ArrayList<String> al = new ArrayList<>();
			al.add("1. Lose resistance");
			al.add("2. Use stop card");
			OptionsMessage om = MessageFactory.createOptionsMessageBattlecry(al);
			BroadcastOptions(om);
			done = true;
		} else if (cardID >= 71 && cardID <= 74) {// Tea ceremony
			pOwn.addXCards(croupier.getXCards(2));
			for (int i = 0; i < players.size(); i++) {
				players.get(i).addCard(croupier.getOneCard());
				table.sendMessageBroadcast(createUpdateMessage(players.get(i)));
			}
			done = true;

		}
		return true;
	}

	private void BroadcastOptions(OptionsMessage om) {
		optionsPending += players.size();
		table.sendMessageBroadcast(om);

	}

	/**
	 * Find card from the table and put it in the waste stack
	 * 
	 * @param cardID
	 *            card tu put in the waste stack
	 */
	private void findAndputCardInWaste(int cardID) {
		// TODO Auto-generated method stub

	}

	private PlayerUpdateMessage createUpdateMessage(Player p) {
		ArrayList<Integer> al = new ArrayList<>();
		for (int i = 0; i < p.getHand().size(); i++) {
			al.add(p.getHand().get(i).getCardID());
		}
		ArrayList<Integer> al2 = new ArrayList<>();
		for (int i = 0; i < p.getPenalty().size(); i++) {
			al2.add(p.getPenalty().get(i).getCardID());
		}
		PlayerUpdateMessage rtn = MessageFactory.createUpdate(p.getSeat(), p.getHonor(), p.getResistence(),
				p.isOfensive(), p.getArmor(), p.getExtraDificulty(), al, p.getExtraDamage(), al2);

		return rtn;
	}

	private int calculateDistance(int ownSeat, int objectiveSeat) {
		int rightSideDistance = 0;
		int currentPos = ownSeat;
		while (currentPos != objectiveSeat) {
			currentPos = getNextSeatPID(currentPos);
			rightSideDistance++;
		}
		return Math.min(rightSideDistance, players.size() - rightSideDistance)
				+ players.get(objectiveSeat).getExtraDificulty();
	}

	/**
	 * Adds a player to the table.
	 * 
	 * @param player
	 *            Player to be added
	 * @param seat
	 *            where the player is to be sat
	 * @return true if possible and done, false otherwise
	 */
	public boolean addPlayer(Player player, int seat) {
		if (seat >= 0 && seat < players.size() + 1) {
			System.out.println("Entrying en addplayer, seat: " + seat + " playerSize: " + players.size());
			players.add(player);
			// this.players.set(seat, player);
			player.setOfensive(false);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Removes a player from the table.
	 * 
	 * @param player
	 *            , player to be removed.
	 * @return true if removed, false if player wasn't at the table.
	 */
	public boolean removePlayer(Player player) {
		boolean playerFound = false;
		int position = getPlayerPosition(player);
		if (position != -1) {
			players.remove(position);
			playerFound = true;
		}
		return playerFound;
	}

	/**
	 * Removes the player with the given ID from the table.
	 * 
	 * @param playerId
	 * @return true if possible and done, false otherwise
	 */
	public boolean removePlayer(int playerId) {
		return removePlayer(findPlayerById(playerId));
	}

	/**
	 * Gets player position at the table.
	 * 
	 * @param player
	 *            Player to get the position
	 * @return player's position, -1 if there's no match
	 */
	public int getPlayerPosition(Player player) {
		return players.indexOf(player);
	}

	/**
	 * Finds the player with the given ID.
	 * 
	 * @param playerId
	 *            pid of the player to find
	 * @return Payer if exists, null otherwise
	 */
	public Player findPlayerById(int playerId) {
		return isBounds(playerId) ? players.get(playerId) : null;
	}

	/**
	 * Finds out whether the given id inbounds the table.
	 * 
	 * @param playerId
	 *            pid of the player to find
	 * @return boolean
	 */
	public boolean isBounds(int playerId) {
		return playerId >= 0 && playerId < players.size();
	}

	/**
	 * Sets the player holding the turn after a round.
	 */
	public void nextTurn() {
		playerCurrentTurn = players.get(getNextTurnPlayerPID());
		playerCurrentTurn.recoverResistence();
		table.sendNextTurnMsg();
		System.out.println("Called nextTurn in Game");
	}

	/**
	 * This method is used to how if the next turn is the shogun turn
	 * 
	 * @return boolean True if the next player is the shogun
	 */
	public boolean isRoundEnd() {
		int pidNext = getNextTurnPlayerPID();
		// TODO Check how to calculate if the round is ended
		return pidNext == shogunID;
	}

	/**
	 * Get the next turn pid
	 * 
	 * @return next pid to play
	 */
	private int getNextTurnPlayerPID() {
		return getNextSeatPID(getCurrentTurnPID());
	}

	/**
	 * Get the current pid playing
	 * 
	 * @return PID of the player who is playing
	 */
	public int getCurrentTurnPID() {
		return playerCurrentTurn.getSeat();
	}

	/**
	 * Given a seat returns the next position in the table
	 * 
	 * @param currentSeat
	 *            seat where to get next seat
	 * @return Seat number next to play
	 */
	public int getNextSeatPID(int currentSeat) {
		int ret = -1;
		if (currentSeat == players.size() - 1) {
			ret = 0;
		} else {
			ret = ++currentSeat;
		}
		return ret;
	}

	/**
	 * Finds out whether the current game has ended.
	 * 
	 * @return boolean True if the game ended
	 */
	public boolean isGameEnd() {
		for (Player player : players) {
			if (player.getHonor() == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Takes all necessary actions after a game ands.
	 */
	public void onGameEnd() {
		System.out.println("The has ended!!");
		int teamNinja = 0, teamShogun = 0;
		for (int i = 0; i < players.size(); i++) {
			Player p = players.get(i);
			int currentHonor = p.getHonor();
			// TODO implement Daimio card (extra honor if have that card inthe
			// hand)
			// TODO implement Mortal hit (killed by the member of the same team)
			switch (p.getRole()) {
			case NINJA: // TODO pending to change for diferent ninja types
				currentHonor *= 2;

				teamNinja += currentHonor;
				break;
			case SAMURAI:
				currentHonor *= 2;
				teamShogun += currentHonor;
				break;
			case SHOGUN:
				currentHonor *= 2;
				teamShogun += currentHonor;
				break;
			default:
				break;
			}

		}
		String msg = null;
		if (teamNinja >= teamShogun) {
			msg = "The winner is the Ninja team!";
		} else {
			msg = "The winner is the Shogun team!";
		}

		msg.concat("  teamNinja = " + teamNinja + " & teamShogun = " + teamShogun);

		table.sendMessageBroadcast(MessageFactory.createErrorMessage(msg)); // TODO
																			// temporary
																			// the
																			// winner
																			// is
																			// by
																			// error
																			// message
		table.finish();
	}

	/**
	 * Starts dealing cards, and setting the first player
	 */
	public void initGame() {
		if (TEST) {
			System.err.println("Test init");
			croupier.dealCharacters();
			croupier.dealRoles();
			if (playerCurrentTurn == null) {
				int m = findShogunID();
				System.out.println("shogunID: " + m);
				System.out.println("playersSize " + players.size());
				this.playerCurrentTurn = players.get(m);
			}
			croupier.dealHonorAndResistence();
			croupier.testDeal();
		} else {
			croupier.shuffle();
			croupier.dealCharacters();
			croupier.dealRoles();

			if (playerCurrentTurn == null) {
				int m = findShogunID();
				System.out.println("shogunID: " + m);
				System.out.println("playersSize " + players.size());
				this.playerCurrentTurn = players.get(m);
			}
			croupier.dealHonorAndResistence();
			croupier.startDeal();
		}

	}

	/**
	 * Find the shogun sitting position
	 * 
	 * @return the shogun sitting position
	 */
	public int findShogunID() {
		boolean found = false;
		int i = -1;
		for (i = 0; i < players.size() && !found; i++) {
			Player p = players.get(i);
			switch (p.getRole()) {
			case NINJA:
				System.out.println("NINJA");
				break;
			case SHOGUN:
				System.out.println("SHOGUN");
				break;
			case SAMURAI:
				System.out.println("SAMURAI");
				break;
			case RONIN:
				System.out.println("RONIN");
				break;
			default:
				break;
			}
			if (p.getRole() == Roles.SHOGUN) {
				found = true;
				break;
			}
		}
		if (i == players.size()) {
			System.err.println("Shogun not found");
		} else {
			System.out.println("Shogun is at the seat : " + i);
		}
		return i;
	}

	// ---------- GETTERS AND SETTERS ----------

	public Croupier getCroupier() {
		return croupier;
	}

	public void setCroupier(Croupier croupier) {
		this.croupier = croupier;
	}

	public void setTable(Table t) {
		this.table = t;
	}

	public Table getTable() {
		return this.table;
	}

	public Player getPlayerCurrentTurn() {
		return playerCurrentTurn;
	}

	public void setPlayerCurrentTurn(Player playerCurrentTurn) {
		this.playerCurrentTurn = playerCurrentTurn;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}

	public int getShogunID() {
		return shogunID;
	}

	public void setShogunID(int shogunID) {
		this.shogunID = shogunID;
	}

	public int getOptionsPending() {
		return optionsPending;
	}

	public void setOptionsPending(int optionsPending) {
		this.optionsPending = optionsPending;
	}

}
