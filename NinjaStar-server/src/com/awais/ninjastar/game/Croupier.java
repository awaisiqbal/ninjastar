/* Croupier.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.awais.ninjastar.hand.ActionCard;
import com.awais.ninjastar.hand.Card;
import com.awais.ninjastar.hand.CardType;
import com.awais.ninjastar.hand.PropertyCard;
import com.awais.ninjastar.hand.WeaponCard;
import com.awais.ninjastar.player.Characters;
import com.awais.ninjastar.player.Player;
import com.awais.ninjastar.player.Roles;

/**
 * Class that models a Croupier in a Game. This classes handles the card dealing
 * and player status.
 */
public class Croupier {

	/**
	 * Game where this crouper is working
	 */
	private Game game;

	/**
	 * Cards stack
	 */
	private List<Card> stack;

	/**
	 * Waste cards stack
	 */
	private List<Card> wasteStack;

	/**
	 * Random generator
	 */
	private Random randomGenerator;

	public Croupier(Game game) {
		this.game = game;
		this.stack = new ArrayList<Card>();
		this.wasteStack = new ArrayList<Card>();
		randomGenerator = new Random();
		setStack();
	}

	/**
	 * Generate a brand new stack with all the cards
	 */
	public void setStack() {
		this.stack.clear();
		manuallyaddCards();

	}

	/**
	 * Temporary add all the cards Manually, any change here also should be
	 * changed in Card.findCardById()
	 */
	private void manuallyaddCards() {
		loadWeaponsCards();
		loadPropertyCards();
		loadActionCards();
	}

	/**
	 * Deals Characters cards to the players randomly
	 */
	public void dealCharacters() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < game.MAX_CAPACITY; i++) {
			list.add(i);
		}
		Collections.shuffle(list, new Random());
		for (int i = 0; i < list.size(); i++) {
			System.err.println(list.get(i));
		}
		List<Player> list2 = game.getPlayers();
		for (int i = 0; i < list2.size(); i++) {
			list2.get(i).setPj(Characters.values()[list.get(i)]);
		}
	}

	/**
	 * Deals the roles cards to the players randomly
	 */
	public void dealRoles() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (game.MAX_CAPACITY == 4) {
			list.add(0); // Add Shogun
			list.add(1); // Add samurai
			list.add(2); // Add Ninja
			list.add(2); // Add Ninja
		} else {
			System.err.println("dealRoles solo esta pra 4 jugadores");
		}
		Collections.shuffle(list);

		List<Player> list2 = game.getPlayers();
		for (int i = 0; i < list2.size(); i++) {
			Player p = list2.get(i);
			Roles r = Roles.values()[list.get(i)];
			p.setRole(r);
		}
	}

	/**
	 * Gives 4 cards to each player in the table
	 */
	public void startDeal() {
		// TODO future feature check how many cards to deal for each role
		int numberOfCards = 4;
		List<Player> list = game.getPlayers();
		for (int j = 0; j < list.size(); j++) {
			list.get(j).addXCards(getXCards(numberOfCards));
		}
	}

	/**
	 * Gives Honor and Resistance points to each player depending to ROle and
	 * Character
	 */
	public void dealHonorAndResistence() {
		List<Player> players = game.getPlayers();
		for (Player player : players) {
			if (player.getRole() == Roles.SHOGUN) {
				player.setHonor(5);
			} else {
				player.setHonor(3);// TODO 4 if is with 5 or 6 players
			}
			dealResistence(player);
		}
	}

	/**
	 * Gives resistance points to the Player p depending in he's Character
	 * 
	 * @param p
	 *            Player who needs the resistance points
	 */
	public void dealResistence(Player p) {
		switch (p.getPj()) {
		case BENKEI:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case CHIYOME:
			p.setResistence(4);
			p.setMax_resistence(4);
			break;
		case GOEMON:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case GINCHIYO:
			p.setResistence(4);
			p.setMax_resistence(5);
			break;
		case HANZO:
			p.setResistence(4);
			p.setMax_resistence(4);
			break;
		case HIDEYOSHI:
			p.setResistence(4);
			p.setMax_resistence(4);
			break;
		case IEYASU:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case KOJIRO:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case MUSASHI:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case NOBUNAGA:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case TOMOE:
			p.setResistence(5);
			p.setMax_resistence(5);
			break;
		case USHIWAKA:
			p.setResistence(4);
			p.setMax_resistence(4);
			break;
		}
	}

	/**
	 * Deals two cards to the current turn player
	 */
	public void deal2CardsCurrentTurn() {
		Card card1 = getOneCard();
		Card card2 = getOneCard();
		game.getPlayerCurrentTurn().addCard(card1);
		game.getPlayerCurrentTurn().addCard(card2);
	}

	/**
	 * Sets the players' hand empty before a new game.
	 */
	public void resetCards() {
		this.stack = new ArrayList<Card>();
		this.wasteStack = new ArrayList<Card>();
		randomGenerator = new Random();
		for (int i = 0; i < game.getPlayers().size(); i++) {
			Player p = game.getPlayers().get(i);
			p.setHand(new ArrayList<>());
		}
		setStack();

	}

	/**
	 * Takes one random card from the stack, and removes the card from the
	 * stack. Used for turn and river rounds, and for dealing the cards to the
	 * players as well.
	 * 
	 * @return one random card from the stack
	 */
	public Card getOneCard() {
		if (stack.size() == 0) {
			System.err.println("Empty Stack");
			stack = new ArrayList<>(wasteStack);
			wasteStack = new ArrayList<>();
		}
		int index = randomGenerator.nextInt(stack.size());
		Card ret = stack.get(index);
		stack.remove(index);
		return ret;
	}

	/**
	 * Gets n cards from the stack
	 * 
	 * @param n
	 *            Numbers of cards to get from the stack
	 * @return List of n cards
	 */
	public List<Card> getXCards(int n) {
		List<Card> tmp = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			tmp.add(getOneCard());
		}
		return tmp;
	}

	/**
	 * Adds a Card to the waste stack
	 * 
	 * @param c
	 */
	public void putWaste(Card c) {
		wasteStack.add(c);
	}

	/**
	 * Get a random Card from the waste stack
	 * 
	 * @return Card getted from the waste stack
	 */
	public Card getOneWasteCard() {
		int index = randomGenerator.nextInt(wasteStack.size());
		Card ret = wasteStack.get(index);
		wasteStack.remove(index);
		return ret;
	}

	/**
	 * Shuffles the stack before a new game starts.
	 */
	public void shuffle() {
		Collections.shuffle(this.stack);
	}

	public List<Card> getStack() {
		return stack;
	}

	public void setStack(List<Card> stack) {
		this.stack = stack;
	}

	/**
	 * Loads all the Weapon cards
	 */
	private void loadWeaponsCards() {
		Card c1 = new WeaponCard(0, "Bo", CardType.WEAPON, 1, 2);
		stack.add(c1);
		c1 = new WeaponCard(1, "Bo", CardType.WEAPON, 1, 2);
		stack.add(c1);
		c1 = new WeaponCard(2, "Bo", CardType.WEAPON, 1, 2);
		stack.add(c1);
		c1 = new WeaponCard(3, "Bo", CardType.WEAPON, 1, 2);
		stack.add(c1);
		c1 = new WeaponCard(4, "Bo", CardType.WEAPON, 1, 2);
		stack.add(c1);

		c1 = new WeaponCard(5, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);
		c1 = new WeaponCard(6, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);
		c1 = new WeaponCard(7, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);
		c1 = new WeaponCard(8, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);
		c1 = new WeaponCard(9, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);
		c1 = new WeaponCard(10, "Bokken", CardType.WEAPON, 1, 1);
		stack.add(c1);

		c1 = new WeaponCard(11, "Kusarigama", CardType.WEAPON, 2, 2);
		stack.add(c1);
		c1 = new WeaponCard(12, "Kusarigama", CardType.WEAPON, 2, 2);
		stack.add(c1);
		c1 = new WeaponCard(13, "Kusarigama", CardType.WEAPON, 2, 2);
		stack.add(c1);
		c1 = new WeaponCard(14, "Kusarigama", CardType.WEAPON, 2, 2);
		stack.add(c1);

		c1 = new WeaponCard(15, "Nagayari", CardType.WEAPON, 2, 4);
		stack.add(c1);

		c1 = new WeaponCard(16, "Kiseru", CardType.WEAPON, 2, 1);
		stack.add(c1);
		c1 = new WeaponCard(17, "Kiseru", CardType.WEAPON, 2, 1);
		stack.add(c1);
		c1 = new WeaponCard(18, "Kiseru", CardType.WEAPON, 2, 1);
		stack.add(c1);
		c1 = new WeaponCard(19, "Kiseru", CardType.WEAPON, 2, 1);
		stack.add(c1);
		c1 = new WeaponCard(20, "Kiseru", CardType.WEAPON, 2, 1);
		stack.add(c1);

		c1 = new WeaponCard(21, "Katana", CardType.WEAPON, 3, 2);
		stack.add(c1);

		c1 = new WeaponCard(22, "Wakizashi", CardType.WEAPON, 3, 1);
		stack.add(c1);

		c1 = new WeaponCard(23, "Shuriken", CardType.WEAPON, 1, 3);
		stack.add(c1);
		c1 = new WeaponCard(24, "Shuriken", CardType.WEAPON, 1, 3);
		stack.add(c1);
		c1 = new WeaponCard(25, "Shuriken", CardType.WEAPON, 1, 3);
		stack.add(c1);

		c1 = new WeaponCard(26, "Daikyu", CardType.WEAPON, 2, 5);
		stack.add(c1);

		c1 = new WeaponCard(27, "Naginata", CardType.WEAPON, 1, 4);
		stack.add(c1);
		c1 = new WeaponCard(28, "Naginata", CardType.WEAPON, 1, 4);
		stack.add(c1);

		c1 = new WeaponCard(29, "Kanabo", CardType.WEAPON, 2, 3);
		stack.add(c1);

		c1 = new WeaponCard(30, "Tanegashima", CardType.WEAPON, 1, 5);
		stack.add(c1);

		c1 = new WeaponCard(31, "Nodachi", CardType.WEAPON, 3, 3);
		stack.add(c1);
	}

	/**
	 * Loads all the Property cards
	 */
	private void loadPropertyCards() {
		Card c1 = new PropertyCard(32, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(33, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(34, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(35, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(36, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(37, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		stack.add(c1);
		c1 = new PropertyCard(38, "Bushido", CardType.PROPERTY,
				"Play against any player, In you turn get a card from the stack if it's a weapon waste one of you weapons or lose 1 Honor and waste this card, otherwise waste this card and pass Bushido to next player");
		stack.add(c1);
		c1 = new PropertyCard(39, "Bushido", CardType.PROPERTY,
				"Play against any player, In you turn get a card from the stack if it's a weapon waste one of you weapons or lose 1 Honor and waste this card, otherwise waste this card and pass Bushido to next player");
		stack.add(c1);
		c1 = new PropertyCard(40, "Fast unsheathed", CardType.PROPERTY, "You weapons does 1 extra damage");
		stack.add(c1);
		c1 = new PropertyCard(41, "Fast unsheathed", CardType.PROPERTY, "You weapons does 1 extra damage");
		stack.add(c1);
		c1 = new PropertyCard(42, "Fast unsheathed", CardType.PROPERTY, "You weapons does 1 extra damage");
		stack.add(c1);
		c1 = new PropertyCard(43, "Armor", CardType.PROPERTY, "You have a extra Dificulty when some atack you");
		stack.add(c1);
		c1 = new PropertyCard(44, "Armor", CardType.PROPERTY, "You have a extra Dificulty when some atack you");
		stack.add(c1);
		c1 = new PropertyCard(45, "Armor", CardType.PROPERTY, "You have a extra Dificulty when some atack you");
		stack.add(c1);
		c1 = new PropertyCard(46, "Armor", CardType.PROPERTY, "You have a extra Dificulty when some atack you");
		stack.add(c1);
	}

	/**
	 * Loads all the Action cards
	 */
	private void loadActionCards() {
		Card c1 = new ActionCard(47, "Distraction", CardType.ACTION, "Get a card from the Stack");
		stack.add(c1);
		c1 = new ActionCard(48, "Distraction", CardType.ACTION, "Get a card from the Stack");
		stack.add(c1);
		c1 = new ActionCard(49, "Distraction", CardType.ACTION, "Get a card from the Stack");
		stack.add(c1);
		c1 = new ActionCard(50, "Distraction", CardType.ACTION, "Get a card from the Stack");
		stack.add(c1);
		c1 = new ActionCard(51, "Distraction", CardType.ACTION, "Get a card from the Stack");
		stack.add(c1);

		c1 = new ActionCard(52, "Daimio", CardType.ACTION,
				"Get 2 cards from the Stack, if you have this card in you hand at the end you get 1 honor extra");
		stack.add(c1);
		c1 = new ActionCard(53, "Daimio", CardType.ACTION,
				"Get 2 cards from the Stack, if you have this card in you hand at the end you get 1 honor extra");
		stack.add(c1);
		c1 = new ActionCard(54, "Daimio", CardType.ACTION,
				"Get 2 cards from the Stack, if you have this card in you hand at the end you get 1 honor extra");
		stack.add(c1);

		c1 = new ActionCard(55, "Breathing", CardType.ACTION,
				"Restore all you Resistence points and select 1 player to get card from the stack");
		stack.add(c1);
		c1 = new ActionCard(56, "Breathing", CardType.ACTION,
				"Restore all you Resistence points and select 1 player to get card from the stack");
		stack.add(c1);
		c1 = new ActionCard(57, "Breathing", CardType.ACTION,
				"Restore all you Resistence points and select 1 player to get card from the stack");
		stack.add(c1);

		c1 = new ActionCard(58, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);
		c1 = new ActionCard(59, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);
		c1 = new ActionCard(60, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);
		c1 = new ActionCard(61, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);
		c1 = new ActionCard(62, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);
		c1 = new ActionCard(63, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		stack.add(c1);

		c1 = new ActionCard(64, "Jiu-jitsu", CardType.ACTION,
				"Rest of the players must choose lose 1 weapon or 1 Resistence");
		stack.add(c1);
		c1 = new ActionCard(65, "Jiu-jitsu", CardType.ACTION,
				"Rest of the players must choose lose 1 weapon or 1 Resistence");
		stack.add(c1);
		c1 = new ActionCard(66, "Jiu-jitsu", CardType.ACTION,
				"Rest of the players must choose lose 1 weapon or 1 Resistence");
		stack.add(c1);

		c1 = new ActionCard(67, "Battle Cry", CardType.ACTION,
				"Rest of the users most select between lose 1 Resistence or use Stop Card");
		stack.add(c1);
		c1 = new ActionCard(68, "Battle Cry", CardType.ACTION,
				"Rest of the users most select between lose 1 Resistence or use Stop Card");
		stack.add(c1);
		c1 = new ActionCard(69, "Battle Cry", CardType.ACTION,
				"Rest of the users most select between lose 1 Resistence or use Stop Card");
		stack.add(c1);
		c1 = new ActionCard(70, "Battle Cry", CardType.ACTION,
				"Rest of the users most select between lose 1 Resistence or use Stop Card");
		stack.add(c1);

		c1 = new ActionCard(71, "Tea ceremony", CardType.ACTION,
				"Get 3 cards from the stack, rest of the players gets 1 card from the stack");
		stack.add(c1);
		c1 = new ActionCard(72, "Tea ceremony", CardType.ACTION,
				"Get 3 cards from the stack, rest of the players gets 1 card from the stack");
		stack.add(c1);
		c1 = new ActionCard(73, "Tea ceremony", CardType.ACTION,
				"Get 3 cards from the stack, rest of the players gets 1 card from the stack");
		stack.add(c1);
		c1 = new ActionCard(74, "Tea ceremony", CardType.ACTION,
				"Get 3 cards from the stack, rest of the players gets 1 card from the stack");
		stack.add(c1);

		for (int i = 75; i <= 89; i++) {
			c1 = new ActionCard(i, "Stop", CardType.ACTION, "Blocks any attack");
			stack.add(c1);
		}

	}

	public void testDeal() {
		List<Player> list = game.getPlayers();
		for (int j = 0; j < list.size(); j++) {
			list.get(j).addCard(new ActionCard(74, "Tea ceremony", CardType.ACTION,
					"Get 3 cards from the stack, rest of the players gets 1 card from the stack"));
			list.get(j).addCard(new ActionCard(74, "Tea ceremony", CardType.ACTION,
					"Get 3 cards from the stack, rest of the players gets 1 card from the stack"));
			list.get(j).addCard(new ActionCard(74, "Tea ceremony", CardType.ACTION,
					"Get 3 cards from the stack, rest of the players gets 1 card from the stack"));
			list.get(j).addCard(new ActionCard(74, "Tea ceremony", CardType.ACTION,
					"Get 3 cards from the stack, rest of the players gets 1 card from the stack"));
			// list.get(j).addXCards(getXCards(numberOfCards));
		}

	}

}
