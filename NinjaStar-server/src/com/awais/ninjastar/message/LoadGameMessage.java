/* LoadGameMessage.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.message;

import java.util.ArrayList;
import java.util.List;

public class LoadGameMessage extends AbstractMessage {

	/**
	 * Name of all the players
	 */
	private List<String> Names;
	private List<String> Roles;
	private List<String> Characters;
	private ArrayList<ArrayList<Integer>> Hands;
	private List<Integer> Armors;
	private List<Integer> ExtraDificultys;
	private List<Integer> Honors;
	private List<Integer> Resistences;

	@Override
	public int getCode() {
		return MessageFactory.MSG_LOADGAME;
	}

	public LoadGameMessage(List<String> names, List<String> roles, List<String> characters,
			ArrayList<ArrayList<Integer>> hands2, List<Integer> armors, List<Integer> extraDificultys,
			List<Integer> honors, List<Integer> resistences) {
		super();
		Names = names;
		Roles = roles;
		Characters = characters;
		Hands = hands2;
		Armors = armors;
		ExtraDificultys = extraDificultys;
		Honors = honors;
		Resistences = resistences;
	}

	public LoadGameMessage() {
		super();
	}

	public List<String> getNames() {
		return Names;
	}

	public void setNames(List<String> names) {
		Names = names;
	}

	public List<String> getRoles() {
		return Roles;
	}

	public void setRoles(List<String> roles) {
		Roles = roles;
	}

	public List<String> getCharacters() {
		return Characters;
	}

	public void setCharacters(List<String> characters) {
		Characters = characters;
	}

	public ArrayList<ArrayList<Integer>> getHands() {
		return Hands;
	}

	public void setHands(ArrayList<ArrayList<Integer>> hands) {
		Hands = hands;
	}

	public List<Integer> getArmors() {
		return Armors;
	}

	public void setArmors(List<Integer> armors) {
		Armors = armors;
	}

	public List<Integer> getExtraDificultys() {
		return ExtraDificultys;
	}

	public void setExtraDificultys(List<Integer> extraDificultys) {
		ExtraDificultys = extraDificultys;
	}

	public List<Integer> getHonors() {
		return Honors;
	}

	public void setHonors(List<Integer> honors) {
		Honors = honors;
	}

	public List<Integer> getResistences() {
		return Resistences;
	}

	public void setResistences(List<Integer> resistences) {
		Resistences = resistences;
	}

}
