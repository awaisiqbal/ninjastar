/* OptionsMessage.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.message;

import java.util.ArrayList;

public class OptionsMessage extends AbstractMessage {

	

	/**
	 * ID used to identify a option
	 */
	private int optionsID;

	/**
	 * First options by default
	 */
	private ArrayList<String> options;

	/**
	 * Selected option
	 */
	private int selectedOption = 0;
	
	public static final int ATTACK = 0;
	public static final int JIUJITSU = 1;
	public static final int BUSHIDO = 2;
	public static final int BATTLECRY = 3;
	public static final int BREATHING = 4;
	public static final int GEISHA = 5;

	public OptionsMessage(int optionsID, ArrayList<String> ops) {
		super();
		this.optionsID = optionsID;
		this.options = ops;
	}

	@Override
	public int getCode() {
		return MessageFactory.MSG_OPTIONS;
	}

	public int getOptionsID() {
		return optionsID;
	}

	public void setOptionsID(int optionsID) {
		this.optionsID = optionsID;
	}

	public ArrayList<String> getOptions() {
		return options;
	}

	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}

	public int getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}

}
