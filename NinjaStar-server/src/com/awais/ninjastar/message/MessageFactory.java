/* GameStartMessage.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.message;

import java.util.ArrayList;

import com.awais.ninjastar.player.CharactersRolesUtility;
import com.awais.ninjastar.player.Player;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * An implementation of the Factory pattern. This class lets you create a
 * Message from a json String. Also it contains methods that give you a
 * reference to a Message that holds an instance to any of its subclasses.
 * 
 * @author Awais Iqbal Begum Begum
 * 
 */
public class MessageFactory {

	/**
	 * The Message codes.
	 */
	public static final int MSG_PLAYERUPDATE = 0;
	public static final int MSG_DISCONNECT = 1;
	public static final int MSG_GAMEEND = 2;
	public static final int MSG_GAMESTART = 3;
	public static final int MSG_HANDSHAKE = 4;
	public static final int MSG_LOADGAME = 5;
	public static final int MSG_NULL = 6;
	public static final int MSG_TURNEND = 7;
	public static final int MSG_TURNSTART = 8;
	public static final int MSG_CARDUSAGE = 9;
	public static final int MSG_ERRORMESSAGE = 10;
	public static final int MSG_OPTIONS = 11;

	private static JsonParser mParser = new JsonParser();

	/**
	 * Given a Message encoded in json format returns the Message instance that
	 * represents it.
	 * 
	 * @param json
	 *            A String in json format created from Message.toJson()
	 * @return Message instance.
	 */
	public static Message create(String json) {
		// A reference to the Message instance that will be returned.
		Message msg = null;
		Gson gson = new Gson();

		// Get the Message.getCode()
		JsonElement obj = mParser.parse(json);
		final int code = obj.getAsJsonObject().get("code").getAsInt();

		// Create the corresponding instance.
		switch (code) {
		case MessageFactory.MSG_DISCONNECT:
			msg = gson.fromJson(json, DisconnectMessage.class);
			break;
		case MessageFactory.MSG_GAMEEND:
			msg = gson.fromJson(json, GameEndMessage.class);
			break;
		case MessageFactory.MSG_GAMESTART:
			msg = gson.fromJson(json, GameStartMessage.class);
			break;
		case MessageFactory.MSG_HANDSHAKE:
			msg = gson.fromJson(json, HandshakeMessage.class);
			break;
		case MessageFactory.MSG_LOADGAME:
			msg = gson.fromJson(json, LoadGameMessage.class);
			break;
		case MessageFactory.MSG_NULL:
			msg = gson.fromJson(json, NullMessage.class);
			break;
		case MessageFactory.MSG_TURNEND:
			msg = gson.fromJson(json, TurnEndMessage.class);
			break;
		case MessageFactory.MSG_TURNSTART:
			msg = gson.fromJson(json, TurnStartMessage.class);
			break;
		case MessageFactory.MSG_PLAYERUPDATE:
			msg = gson.fromJson(json, PlayerUpdateMessage.class);
			break;
		case MessageFactory.MSG_CARDUSAGE:
			msg = gson.fromJson(json, CardUsageMessage.class);
			break;
		case MessageFactory.MSG_ERRORMESSAGE:
			msg = gson.fromJson(json, ErrorMessage.class);
			break;
		case MessageFactory.MSG_OPTIONS:
			msg = gson.fromJson(json, OptionsMessage.class);
			break;
		default:
			System.err.println("Mensaje no Identified!");
			break;
		}

		return msg;

	}

	public static Message createDisconnectMessage() {
		return new DisconnectMessage();
	}

	public static Message createNullMessage() {
		return new NullMessage();
	}

	public static LoadGameMessage createLoadGameMessage(ArrayList<Player> players) {
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> roles = new ArrayList<>();
		ArrayList<String> characters = new ArrayList<>();
		ArrayList<ArrayList<Integer>> hands = new ArrayList<>();
		ArrayList<Integer> armors = new ArrayList<>();
		ArrayList<Integer> extradificulty = new ArrayList<>();
		ArrayList<Integer> honors = new ArrayList<>();
		ArrayList<Integer> resistences = new ArrayList<>();
		for (int i = 0; i < players.size(); i++) {
			Player p = players.get(i);
			names.add(p.getProfile().getName());
			roles.add(CharactersRolesUtility.CharactersToString(p.getRole()));
			characters.add(CharactersRolesUtility.CharactersToString(p.getPj()));
			ArrayList<Integer> al = new ArrayList<>();
			for (int j = 0; j < p.getHand().size(); j++) {
				al.add(p.getHand().get(j).getCardID());
			}
			hands.add(al);
			armors.add(p.getArmor());
			extradificulty.add(p.getExtraDificulty());
			honors.add(p.getHonor());
			resistences.add(p.getResistence());
		}
		LoadGameMessage lgm = new LoadGameMessage(names, roles, characters, hands, armors, extradificulty, honors,
				resistences);
		return lgm;

	}

	public static TurnStartMessage createTurnStartMessage(int pid) {
		return new TurnStartMessage(pid);
	}

	public static TurnEndMessage createTurnEndMessage(int pid) {
		return new TurnEndMessage(pid);
	}

	public static GameStartMessage createGameStartMessage() {
		return new GameStartMessage();
	}

	public static GameStartMessage createGameStart() {
		return new GameStartMessage();
	}

	public static CardUsageMessage createCardUsage(int cardID) {
		return new CardUsageMessage(CardUsageMessage.SIMPLE, cardID);
	}

	public static CardUsageMessage createCardUsageWithObjectivePlayer(int cardID, int objectivePID) {
		return new CardUsageMessage(CardUsageMessage.OBJECTIVEPID, cardID, objectivePID);
	}

	public static CardUsageMessage createCardUsageWithObjectiveCard(int cardID, int objectivePID) {
		return new CardUsageMessage(CardUsageMessage.OBJECTIVECARD, cardID, objectivePID);
	}

	public static OptionsMessage createOptionsMessage(int id, ArrayList<String> options) {
		return new OptionsMessage(id, options);
	}

	public static ErrorMessage createErrorMessage(String string) {
		return new ErrorMessage(string);
	}

	public static PlayerUpdateMessage createUpdate(int seat, int honor, int resistence, boolean ofensive, int armor,
			int extraDificulty, ArrayList<Integer> hand, int extraDamage, ArrayList<Integer> penaltys) {
		PlayerUpdateMessage pum = new PlayerUpdateMessage(seat, ofensive, armor, extraDificulty, honor, resistence,
				hand, extraDamage, penaltys);
		return pum;
	}

	public static OptionsMessage createOptionsMessageJiuJitsu(ArrayList<String> l) {
		OptionsMessage om = new OptionsMessage(OptionsMessage.JIUJITSU, l);
		return om;
	}

	public static OptionsMessage createOptionsMessageAttack(ArrayList<String> l) {
		OptionsMessage om = new OptionsMessage(OptionsMessage.ATTACK, l);
		return om;
	}

	public static OptionsMessage createOptionsMessageBushido(ArrayList<String> l) {
		OptionsMessage om = new OptionsMessage(OptionsMessage.BUSHIDO, l);
		return om;
	}

	public static OptionsMessage createOptionsMessageBattlecry(ArrayList<String> l) {
		OptionsMessage om = new OptionsMessage(OptionsMessage.BATTLECRY, l);
		return om;
	}

}
