/* PlayerUpdateMessage.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.message;

import java.util.ArrayList;
import java.util.List;

/**
 * This message is used to communicate a update to the player, if any field is
 * null that means that value is not updated, is the same before the message
 * 
 * @author Awais Iqbal Begum
 *
 */
public class PlayerUpdateMessage extends AbstractMessage {

	private Boolean ofensive;
	private Integer armor;
	private Integer extraDificulty;
	private Integer honor;
	private Integer resistence;
	private List<Integer> hand;
	private Integer extraDamage;
	private List<Integer> penaltys;

	public PlayerUpdateMessage(int pid, Boolean ofensive, Integer armor, Integer extraDificulty, Integer honor,
			Integer resistence, List<Integer> hand, int extraDamage, ArrayList<Integer> penaltys) {
		super(pid);
		this.ofensive = ofensive;
		this.armor = armor;
		this.extraDificulty = extraDificulty;
		this.honor = honor;
		this.resistence = resistence;
		this.hand = hand;
		this.extraDamage = extraDamage;
		this.penaltys = penaltys;
	}

	@Override
	public int getCode() {
		return MessageFactory.MSG_PLAYERUPDATE;
	}

	public Boolean getOfensive() {
		return ofensive;
	}

	public void setOfensive(Boolean ofensive) {
		this.ofensive = ofensive;
	}

	public Integer getArmor() {
		return armor;
	}

	public void setArmor(Integer armor) {
		this.armor = armor;
	}

	public Integer getExtraDificulty() {
		return extraDificulty;
	}

	public void setExtraDificulty(Integer extraDificulty) {
		this.extraDificulty = extraDificulty;
	}

	public Integer getHonor() {
		return honor;
	}

	public void setHonor(Integer honor) {
		this.honor = honor;
	}

	public Integer getResistence() {
		return resistence;
	}

	public void setResistence(Integer resistence) {
		this.resistence = resistence;
	}

	public List<Integer> getHand() {
		return hand;
	}

	public void setHand(List<Integer> hand) {
		this.hand = hand;
	}

	public Integer getExtraDamage() {
		return extraDamage;
	}

	public void setExtraDamage(Integer extraDamage) {
		this.extraDamage = extraDamage;
	}

	public List<Integer> getPenaltys() {
		return penaltys;
	}

	public void setPenaltys(List<Integer> penaltys) {
		this.penaltys = penaltys;
	}
	

}
