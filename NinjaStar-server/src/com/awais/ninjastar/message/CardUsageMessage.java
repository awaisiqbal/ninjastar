/* CardUsageMessage.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.message;

/**
 * Message telling that someone used a card
 * 
 * @author Awais Iqbal Begum
 *
 */
public class CardUsageMessage extends AbstractMessage {

	/**
	 * Objective of the card, not always used, only used in the card where are
	 * possible objectives. -1 when there is no objective person
	 */
	private int objectivePID = -1;

	/**
	 * Objective card to remove, -1 when there no objective card
	 */
	private int objectiveCardID = -1;

	/**
	 * ID of the card to use
	 */
	private int usedCardID;

	public final static int SIMPLE = 0;
	public final static int OBJECTIVEPID = 2;
	public final static int OBJECTIVECARD = 1;

	/**
	 * Creator of the {@link CardUsageMessage}
	 * 
	 * @param objectiveType
	 *            Type of the usedCard must be CardUsageMessage.OBJECTIVEPID or
	 *            CardUsageMessage.OBJECTIVECARD
	 * @param cardUsedID
	 *            card usedID
	 * @param objective
	 *            objective of this card
	 */
	public CardUsageMessage(int objectiveType, int cardUsedID, int objective) {
		super();
		if (objectiveType == CardUsageMessage.OBJECTIVECARD) {
			this.objectiveCardID = objective;
		} else if (objectiveType == CardUsageMessage.OBJECTIVEPID) {
			this.objectivePID = objective;
		}

		this.usedCardID = cardUsedID;
	}

	public CardUsageMessage(int objectiveType, int cardUsedID) {
		super();
		this.usedCardID = cardUsedID;
	}

	@Override
	public int getCode() {
		return MessageFactory.MSG_CARDUSAGE;
	}

	public int getObjectivePID() {
		return objectivePID;
	}

	public void setObjectivePID(int objectivePID) {
		this.objectivePID = objectivePID;
	}

	public int getObjectiveCardID() {
		return objectiveCardID;
	}

	public void setObjectiveCardID(int objectiveCardID) {
		this.objectiveCardID = objectiveCardID;
	}

	public int getUsedCardID() {
		return usedCardID;
	}

	public void setUsedCardID(int usedCardID) {
		this.usedCardID = usedCardID;
	}

}
