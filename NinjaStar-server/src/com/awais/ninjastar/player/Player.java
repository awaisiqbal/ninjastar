/* Player.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.player;

import java.util.ArrayList;
import java.util.List;

import com.awais.ninjastar.hand.Card;
import com.awais.ninjastar.hand.CardType;

public class Player {

	/**
	 * Position where the player is sited
	 */
	protected int seat;

	/**
	 * Public profile of this player
	 */
	protected PublicProfile profile;

	/**
	 * Role of the player
	 */
	protected Roles role;
	/**
	 * Character of the player
	 */
	protected Characters pj;

	/**
	 * Tells if this player is offensive
	 */
	protected boolean isOfensive;
	/**
	 * Hand of this player
	 */
	protected List<Card> hand;

	/**
	 * Extra armor of this player
	 */
	protected int armor = 0;
	/**
	 * Extra difficulty to attack this player
	 */
	protected int extraDificulty = 0;
	/**
	 * Attacks used by this player
	 */
	protected int currentAtacksUseds = 0;
	/**
	 * How many attacks can do this player
	 */
	protected int possibleAtacks = 1;
	/**
	 * Extra damage using any weapon
	 */
	protected int extraDamage = 0;

	/**
	 * Honor left of this player
	 */
	protected int honor;
	/**
	 * Number of the resistances
	 */
	protected int resistence;
	/**
	 * Max. Resistances
	 */
	protected int max_resistence;

	/**
	 * Penalty aplied to this player
	 */
	protected List<Card> penalty;
	/**
	 * Benefit cards
	 */
	protected List<Card> benefitsCards;

	public Player() {

		profile = new PublicProfile();
		profile.setName("test");
		this.hand = new ArrayList<>();
		this.penalty = new ArrayList<>();
		this.benefitsCards = new ArrayList<>();

	}

	/**
	 * give the id of the player (siting position)
	 * 
	 * @return position
	 */
	public int getSeat() {
		return this.seat;
	}

	public void setSeat(int id) {
		this.seat = id;
	}

	public PublicProfile getProfile() {
		return profile;
	}

	public void setProfile(PublicProfile profile) {
		this.profile = profile;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Characters getPj() {
		return pj;
	}

	public void setPj(Characters pj) {
		this.pj = pj;
	}

	public boolean isOfensive() {
		return isOfensive;
	}

	public void setOfensive(boolean ofensive) {
		this.isOfensive = ofensive;
	}

	public List<Card> getHand() {
		return hand;
	}

	public void setHand(List<Card> hand) {
		this.hand = hand;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getExtraDificulty() {
		return extraDificulty;
	}

	public void setExtraDificulty(int extraDificulty) {
		this.extraDificulty = extraDificulty;
	}

	public int getHonor() {
		return honor;
	}

	public void setHonor(int honor) {
		this.honor = honor;
	}

	public int getResistence() {
		return resistence;
	}

	public void setResistence(int resistence) {
		this.resistence = resistence;
	}

	public void addCard(Card card) {
		this.hand.add(card);
	}

	public void addXCards(List<Card> l) {
		for (int i = 0; i < l.size(); i++) {
			addCard(l.get(i));
		}
	}

	public boolean removeCard(Card card) {
		return this.hand.remove(card);
		// TODO remove Benefits
	}

	public List<Card> getPenalty() {
		return penalty;
	}

	public void setPenalty(List<Card> penalty) {
		this.penalty = penalty;
	}

	public void addPenalty(Card card) {
		this.penalty.add(card);
	}

	public void removePenalty(Card card) {
		this.penalty.remove(card);
	}

	/**
	 * Receive damage
	 * 
	 * @param damage
	 *            damage to do to this player
	 */
	public void receiveDamage(int damage) {
		if (damage > armor)
			resistence -= (damage - armor);
		if (resistence <= 0)
			this.isOfensive = false;
	}

	public boolean contains(int cardID) {
		Card c = new Card(cardID, "", CardType.ACTION);
		return this.hand.contains(c);
	}

	public int getCurrentAtacksUseds() {
		return currentAtacksUseds;
	}

	public void setCurrentAtacksUseds(int currentAtacksUseds) {
		this.currentAtacksUseds = currentAtacksUseds;
	}

	public int getPossibleAtacks() {
		return possibleAtacks;
	}

	public void setPossibleAtacks(int possibleAtacks) {
		this.possibleAtacks = possibleAtacks;
	}

	/**
	 * When the player reach resistance lose a Honor
	 */
	public void loseResistence() {
		this.resistence--;
		if (resistence == 0)
			honor--;

	}

	public void recoverResistence() {
		this.resistence = max_resistence;
		this.isOfensive = true;
	}

	public int getMax_resistence() {
		return max_resistence;
	}

	public void setMax_resistence(int max_resistence) {
		this.max_resistence = max_resistence;
	}

	public List<Card> getBenefitsCards() {
		return benefitsCards;
	}

	public void setBenefitsCards(List<Card> benefitsCards) {
		this.benefitsCards = benefitsCards;
	}

	public boolean addBenefitCard(Card card) {
		return this.benefitsCards.add(card);
	}
	public boolean removeBenefitCard(Card card) {
		return this.benefitsCards.remove(card);
	}

	public int getExtraDamage() {
		return extraDamage;
	}

	public void setExtraDamage(int extraDamage) {
		this.extraDamage = extraDamage;
	}

	public void restoreResistance() {
		this.resistence = max_resistence;
		System.out.println("Resistance restored!");

	}

}
