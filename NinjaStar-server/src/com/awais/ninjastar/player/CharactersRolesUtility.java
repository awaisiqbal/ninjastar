/* CharactersRolesUtility.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/
package com.awais.ninjastar.player;

/**
 * Class used to convert {@link Character} to {@link String} and {@link String}
 * to {@link Character}, also can convert {@link Roles} to {@link String} and
 * {@link String} to {@link Roles}
 * 
 * @author Awais Iqbal Begum
 *
 */
public class CharactersRolesUtility {
	public static final Roles CreateRole(String role) {
		Roles r = Roles.RONIN;
		switch (role) {
		case "SHOGUN":
			r = Roles.SHOGUN;
			break;
		case "SAMURAI":
			r = Roles.SAMURAI;
			break;
		case "NINJA":
			r = Roles.NINJA;
			break;
		case "RONIN":
			r = Roles.RONIN;
			break;
		}
		return r;
	}

	public static final String CharactersToString(Roles role) {
		String r = "";
		switch (role) {
		case SHOGUN:
			r = "SHOGUN";
			break;
		case SAMURAI:
			r = "SAMURAI";
			break;
		case NINJA:
			r = "NINJA";
			break;
		case RONIN:
			r = "RONIN";
			break;
		}
		return r;
	}

	public static final Characters CreateCharacters(String characters) {
		Characters r = Characters.BENKEI;
		switch (characters) {
		case "BENKEI":
			r = Characters.BENKEI;
			break;
		case "CHIYOME":
			r = Characters.CHIYOME;
			break;
		case "GOEMON":
			r = Characters.GOEMON;
			break;
		case "GINCHIYO":
			r = Characters.GINCHIYO;
			break;
		case "HANZO":
			r = Characters.HANZO;
			break;
		case "HIDEYOSHI":
			r = Characters.HIDEYOSHI;
			break;
		case "IEYASU":
			r = Characters.IEYASU;
			break;
		case "KOJIRO":
			r = Characters.KOJIRO;
			break;
		case "MUSASHI":
			r = Characters.MUSASHI;
			break;
		case "NOBUNAGA":
			r = Characters.NOBUNAGA;
			break;
		case "TOMOE":
			r = Characters.TOMOE;
			break;
		case "USHIWAKA":
			r = Characters.USHIWAKA;
			break;
		}
		return r;
	}

	public static final String CharactersToString(Characters characters) {
		String r = "";
		switch (characters) {
		case BENKEI:
			r = "BENKEI";
			break;
		case CHIYOME:
			r = "CHIYOME";
			break;
		case GOEMON:
			r = "GOEMON";
			break;
		case GINCHIYO:
			r = "GINCHIYO";
			break;
		case HANZO:
			r = "HANZO";
			break;
		case HIDEYOSHI:
			r = "HIDEYOSHI";
			break;
		case IEYASU:
			r = "IEYASU";
			break;
		case KOJIRO:
			r = "KOJIRO";
			break;
		case MUSASHI:
			r = "MUSASHI";
			break;
		case NOBUNAGA:
			r = "NOBUNAGA";
			break;
		case TOMOE:
			r = "TOMOE";
			break;
		case USHIWAKA:
			r = "USHIWAKA";
			break;
		}
		return r;
	}

}
