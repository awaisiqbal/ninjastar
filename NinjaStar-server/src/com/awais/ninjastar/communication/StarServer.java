/* StarServer.java 1.0
 *
 *	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */
package com.awais.ninjastar.communication;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.awais.ninjastar.message.HandshakeMessage;

/**
 * Server which accepts clients to play the game Ninja-star
 * 
 * @author Awais Iqbal Begum
 *
 */
public class StarServer {

	/**
	 * ServerSocket where the users should connect
	 */
	private ServerSocket mServer;

	/**
	 * Port used by the server
	 */
	private static int PORT = 5253;

	/**
	 * All the tables in the game
	 */
	private List<Table> mTables;

	/**
	 * New clients manager
	 */
	private NewClientManagerThread mNewClientThread;

	/**
	 * Number of total tables
	 */
	public int tableCount = 0;

	/**
	 * Number of total users connected
	 */
	public int clientCount = 0;

	public static void main(String args[]) {
		new StarServer();
	}

	public StarServer() {
		mTables = new ArrayList<>();
		try {
			mServer = new ServerSocket(PORT);
			mNewClientThread = new NewClientManagerThread();
			mNewClientThread.start();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error creating the server socket or starting the new client manager");
		}

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				finishAllTables();
			}
		}));

	}

	/**
	 * CLose the current tables
	 */
	protected void finishAllTables() {
		System.err.println("Pendiente de implementar el cierre de todas las mesas");

	}

	/**
	 * Broaadcast a {@link String} to all the {@link ClientThread}
	 * 
	 * @param clients
	 *            Client to send a String
	 * @param output
	 */
	public synchronized void broadcast(List<ClientThread> clients, String output) {
		for (ClientThread clientThread : clients) {
			clientThread.getOut().println(output);
		}
	}

	/**
	 * Sends a individual String a {@link ClientThread}
	 * 
	 * @param client
	 *            user who should receive the message
	 * @param output
	 *            String to send to the user
	 */
	public synchronized void sendMsg(ClientThread client, String output) {
		client.getOut().println(output);
	}

	/**
	 * Remove a table from the game
	 * 
	 * @param table
	 *            {@link Table} to eliminate
	 */
	public void removeTable(Table table) {
		mTables.remove(table);
	}

	/**
	 * Sends a handshake to the client telling which place to sit and the rest
	 * of players information
	 * 
	 * @param ct
	 *            client to inform
	 */
	public void sendHandShake(ClientThread ct) {
		HandshakeMessage handShake = new HandshakeMessage();
		int empty = ct.getmTable().getEmptySeat();
		if (empty == -1) {
			System.err.println("Problem to get a empty seat");
		}
		handShake.setOwnPlayerSeat(empty);
		ct.getOut().println(handShake.toJson());
	}

	/**
	 * Search first empty table
	 * 
	 * @return
	 */
	public Table searchFreeTable() {
		Table t = null;
		for (Table table : mTables) {
			if (table.getmClients().size() < Table.MAX_CAPACITY)
				t = table;
		}
		return t;

	}

	/**
	 * Inner class created to control new threads
	 * 
	 * @author Awais Iqbal Begum
	 *
	 */
	private class NewClientManagerThread extends Thread {
		private boolean end = false;

		@Override
		public void run() {
			while (!end) {
				try {
					Socket cliSocket = mServer.accept();
					if (alreadyConnected(cliSocket)) {
						continue; // no hay que controlar los threads ya
									// conectados
					}
					System.out.println("New client! \t total number of clients: " + clientCount++);
					manageClient(cliSocket); // manage the new socket

				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Error accepting client");
				}
			}
			super.run();
		}

		/**
		 * Given a Socket this method controls the communication between server
		 * and the client
		 * 
		 * @param cliSocket
		 */
		private void manageClient(Socket cliSocket) {
			ClientThread ct = new ClientThread(cliSocket, StarServer.this);
			System.out.println("Thread connected");

			Table emptytable = searchFreeTable();

			if (emptytable == null) {
				System.out.println("New table created...  ID: " + tableCount);
				emptytable = new Table(tableCount, StarServer.this);
				mTables.add(emptytable);
				tableCount++;
			}

			// give client the table information
			ct.setmTable(emptytable);

			// send information to the player
			sendHandShake(ct);

			if (emptytable != null) {
				emptytable.add(ct);
				ct.start();
			}
		}

		/**
		 * Finish the thread
		 */
		@SuppressWarnings("unused")
		public void finish() {
			end = true;
		}

		/**
		 * Method that checks if that socket is already connected or not
		 * 
		 * @param s
		 *            socket to check
		 * @return True if s is already connected
		 */
		public boolean alreadyConnected(Socket s) {
			// TODO need optimization
			for (Table table : mTables) {
				for (ClientThread clTh : table.getmClients()) {
					if (clTh != null && clTh.getSocket().getRemoteSocketAddress().equals(s.getRemoteSocketAddress())) {
						return true;

					}
				}
			}
			return false;
		}

	}

	public void finishGame(Table table) {
		System.out.println("Deleting table" + table.getId());
		this.mTables.remove(table);

	}

}
