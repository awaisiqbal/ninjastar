/* Table.java 1.0
 *
 *	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */
package com.awais.ninjastar.communication;

import java.util.ArrayList;
import java.util.List;

import com.awais.ninjastar.game.Game;
import com.awais.ninjastar.hand.Card;
import com.awais.ninjastar.message.GameStartMessage;
import com.awais.ninjastar.message.LoadGameMessage;
import com.awais.ninjastar.message.Message;
import com.awais.ninjastar.message.MessageFactory;
import com.awais.ninjastar.message.TurnStartMessage;
import com.awais.ninjastar.player.Player;

/**
 * Table where is hapening a {@link Game} with many {@link ClientThread}
 * 
 * @author Awais Iqbal Begum
 *
 */
public class Table {
	/**
	 * ID to identify a table
	 */
	private int id;

	/**
	 * List of the clients connected
	 */
	private ArrayList<ClientThread> mClients = new ArrayList<>();
	/**
	 * Server where this Table is
	 */
	private StarServer mServer;
	/**
	 * Game playing in this table
	 */
	private Game mGame;
	/**
	 * Max players in this table
	 */
	public static final int MAX_CAPACITY = 4;

	public Table(int id, StarServer server) {
		this.id = id;
		this.mServer = server;
		this.mGame = new Game(this);
	}

	/**
	 * Returns the first empty seat in the table
	 * 
	 * @return free position in the table
	 */
	public int getFirstEmptySeat() {
		int ret = -1;
		for (int i = 0; i < mClients.size(); i++) {
			if (mClients.get(i) == null)
				ret = i;
		}
		return ret;
	}

	/**
	 * sends a message to the next player to play, called by Game to start next
	 * turn
	 */
	public void sendNextTurnMsg() {
		int next = mGame.getCurrentTurnPID(); // indicate the seat of the next
												// player
		TurnStartMessage nxtMsg = MessageFactory.createTurnStartMessage(next);
		mServer.broadcast(mClients, nxtMsg.toJson());
	}

	/**
	 * Sends a start message to that player, includes the players cards, id of
	 * the player
	 * 
	 * @param p
	 *            who need to receive the message with the information
	 */
	public void sendStartMsg(Player p) {
		List<Card> pCards = p.getHand();
		GameStartMessage gStart = MessageFactory.createGameStartMessage();
		gStart.setOwnCards(pCards);
		gStart.setPlayerId(p.getSeat());
		mClients.get(p.getSeat()).sendMsg(gStart);
	}

	/**
	 * This method adds a new {@link ClientThread} in the table
	 * 
	 * @param ct
	 *            client to add in the table
	 */
	public void add(ClientThread ct) {
		int seat = getEmptySeat(); // search for the empty seat
		if (seat == -1) {
			System.err.println("no hay asiento");
		}
		ct.setPid(seat); // set the seat number to the client

		Player player = new Player(); // create a new player TODO get the player
										// info
		player.getProfile().setName("player " + seat); // temporary set name by
														// seat
		player.setSeat(seat);

		mClients.add(seat, ct); // add the client in the clients list
		mGame.addPlayer(player, ct.getPid()); // add the new player iun the game
		System.err.println("player: " + player.getProfile().getName() + " seat: " + seat);
		if (countClients() == MAX_CAPACITY) {
			mGame.initGame(); // start the game
			sendLoadGame(); // send the rest of the players information
			sendStartGameMsg(); // send a start game message
			sendNextTurnMsg(); // send the message to the shogun to start

		}
	}

	/**
	 * tells to all the players that the game has started
	 */
	private void sendStartGameMsg() {
		GameStartMessage gsm = MessageFactory.createGameStart();
		sendMessageBroadcast(gsm);
	}

	/**
	 * Sends a message to all clients giving the rest of the players information
	 */
	private void sendLoadGame() {
		LoadGameMessage lgm = MessageFactory.createLoadGameMessage(mGame.getPlayers());
		sendMessageBroadcast(lgm);
	}

	/**
	 * Send a {@link Message} to all the players in the table
	 * 
	 * @param msg
	 *            message to broadcast
	 */
	public void sendMessageBroadcast(Message msg) {
		mServer.broadcast(mClients, msg.toJson());
	}

	/**
	 * Send a {@link Message} to a specific seat
	 * 
	 * @param destinationSeat
	 *            seat where the message should go
	 * @param msg
	 *            Message to send
	 */
	public void sendMessageToSeat(int destinationSeat, Message msg) {
		mServer.sendMsg(mClients.get(destinationSeat), msg.toJson());

	}

	/**
	 * counts the numbers of clients playing
	 * 
	 * @return number of players
	 */
	public int countClients() {
		int counter = 0;
		for (ClientThread cli : mClients) {
			if (cli != null) {
				counter++;
			}
		}
		return counter;
	}

	public int getEmptySeat() {
		int seat = -1;
		if (mClients.size() < MAX_CAPACITY) {
			seat = mClients.size();
		}
		return seat;
	}

	public ArrayList<ClientThread> getmClients() {
		return mClients;
	}

	public void setmClients(ArrayList<ClientThread> mClients) {
		this.mClients = mClients;
	}

	public StarServer getmServer() {
		return mServer;
	}

	public void setmServer(StarServer mServer) {
		this.mServer = mServer;
	}

	public Game getmGame() {
		return mGame;
	}

	public void setmGame(Game mGame) {
		this.mGame = mGame;
	}

	public int getId() {
		return id;
	}

	public void remove(ClientThread clientThread) {
		System.out.println("Deleting a clientThread");
		mClients.remove(clientThread);

	}

	public void finish() {
		mServer.finishGame(this);

	}

}
