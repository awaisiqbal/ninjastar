/* ClientThread.java 1.0
 *
 *	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */
package com.awais.ninjastar.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import com.awais.ninjastar.message.DisconnectMessage;
import com.awais.ninjastar.message.Message;
import com.awais.ninjastar.message.MessageFactory;

/**
 * Represents a user connected to the server
 * 
 * @author Awais Iqbal Begum
 *
 */
public class ClientThread extends Thread {

	/**
	 * Socket to communicate
	 */
	private Socket socket;

	private BufferedReader in;

	private PrintWriter out;

	private boolean end;

	/**
	 * player sitting position
	 */
	private int pid;

	/**
	 * Table where this player is "seated"
	 */
	private Table mTable;

	/**
	 * Reference to the server where is connected
	 */
	private StarServer mServer;

	public ClientThread(Socket cliSocket, StarServer pokerServer) {
		this.mServer = pokerServer;
		socket = cliSocket;
		try {
			cliSocket.setSoTimeout(0);
		} catch (SocketException e1) {
			System.out.println("Error en setSoTimeout del clientThread");
			e1.printStackTrace();
		}

		try {
			in = new BufferedReader(new InputStreamReader(cliSocket.getInputStream()));
		} catch (IOException e) {
			System.out.println("Error creando el IN del clientThread");
			e.printStackTrace();
		}
		try {
			out = new PrintWriter(cliSocket.getOutputStream(), true);
		} catch (IOException e) {
			System.out.println("Error creando el OUT del clientThread");
			e.printStackTrace();

		}
	}

	@Override
	public void run() {
		System.out.println("Running ClientThread...");
		while (!end) {
			try {
				socket.setSoTimeout(0);
			} catch (SocketException e1) {
				e1.printStackTrace();
			}
			String input = null;

			try {
				System.out.println("Reading...");
				input = in.readLine();
			} catch (IOException e) {
				System.out.println("A client has closed the conection , Game is shuting down...");
				input = onDisconnect();
			}

			if (input == null) {
				System.err.println("input is null");
				return;
			}

			if (input != null) {
				Message msg = MessageFactory.create(input);

				msg.setPlayerId(pid);

				Message msgToRet = mTable.getmGame().consumeMessage(msg, pid);

				if (msgToRet != null) {
					sendMsg(msgToRet);
				}
			}

		}

		super.run();
	}

	/**
	 * When the user is disconnected this method close the socket and inform to
	 * the server to disconnect
	 * 
	 * @return String in Json format to seand a message to server
	 */
	public String onDisconnect() {
		System.out.println("Client: " + pid + " is disconnection");
		DisconnectMessage disc = (DisconnectMessage) MessageFactory.createDisconnectMessage();
		disc.setPlayerId(pid);
		end = true;
		mTable.remove(this);
		if (!socket.isClosed()) {
			try {
				socket.shutdownInput();
				socket.shutdownOutput();
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return disc.toJson();
	}

	/**
	 * Message to send to the user
	 */
	public synchronized void sendMsg(Message msg) {
		out.println(msg.toJson());
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public BufferedReader getIn() {
		return in;
	}

	public void setIn(BufferedReader in) {
		this.in = in;
	}

	public PrintWriter getOut() {
		return out;
	}

	public void setOut(PrintWriter out) {
		this.out = out;
	}

	public boolean isEnd() {
		return end;
	}

	public void setEnd(boolean end) {
		this.end = end;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public Table getmTable() {
		return mTable;
	}

	public void setmTable(Table mTable) {
		this.mTable = mTable;
	}

	public StarServer getmServer() {
		return mServer;
	}

	public void setmServer(StarServer mServer) {
		this.mServer = mServer;
	}

}
