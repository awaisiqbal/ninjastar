/* Card.java 1.0
*
*	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
*
*	 The MIT License (MIT)
*
*	 Permission is hereby granted, free of charge, to any person obtaining a copy
*	 of this software and associated documentation files (the \"Software\"), to deal
*	 in the Software without restriction, including without limitation the rights
*	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*	 copies of the Software, and to permit persons to whom the Software is
*	 furnished to do so, subject to the following conditions:
*
*	 The above copyright notice and this permission notice shall be included in
*	 all copies or substantial portions of the Software.
*
*	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*	 THE SOFTWARE.
*/

package com.awais.ninjastar.hand;

/**
 * A class that models a Card in a game.
 * 
 * @author Awais Iqbal Begum
 *
 */
public class Card {
	/**
	 * Indentifier of the card
	 */
	private int cardID;
	/**
	 * Name of the card
	 */
	private String name;
	/**
	 * Card type
	 */
	private CardType type;

	public Card(int cardID, String name, CardType type) {
		this.cardID = cardID;
		this.name = name;
		this.type = type;
	}

	public boolean isWeapon() {
		return this.type == CardType.WEAPON;
	}

	public boolean isProperty() {
		return this.type == CardType.PROPERTY;
	}

	public boolean isAction() {
		return this.type == CardType.ACTION;
	}

	public String toString() {
		return "ID: " + getCardID() + "\tName: " + this.getName();
	}

	public int getCardID() {
		return cardID;
	}

	public void setCardID(int cardID) {
		this.cardID = cardID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CardType getType() {
		return type;
	}

	public void setType(CardType type) {
		this.type = type;
	}

	public static final Card getByID(int id) {
		Card c1 = null;
		if (id >= 0 && id <= 4) {
			c1 = new WeaponCard(id, "Bo", CardType.WEAPON, 1, 2);
		} else if (id >= 5 && id <= 10) {
			c1 = new WeaponCard(id, "Bokken", CardType.WEAPON, 1, 1);
		} else if (id >= 11 && id <= 14) {
			c1 = new WeaponCard(id, "Kusarigama", CardType.WEAPON, 2, 2);
		} else if (id == 15) {
			c1 = new WeaponCard(id, "Nagayari", CardType.WEAPON, 2, 4);
		} else if (id >= 16 && id <= 20) {
			c1 = new WeaponCard(id, "Kiseru", CardType.WEAPON, 2, 1);
		} else if (id == 21) {
			c1 = new WeaponCard(id, "Katana", CardType.WEAPON, 3, 2);
		} else if (id == 22) {
			c1 = new WeaponCard(id, "Wakizashi", CardType.WEAPON, 3, 1);
		} else if (id >= 23 && id <= 25) {
			c1 = new WeaponCard(id, "Shuriken", CardType.WEAPON, 1, 3);
		} else if (id == 26) {
			c1 = new WeaponCard(id, "Daikyu", CardType.WEAPON, 2, 5);
		} else if (id >= 27 && id <= 28) {
			c1 = new WeaponCard(id, "Naginata", CardType.WEAPON, 1, 4);
		} else if (id == 29) {
			c1 = new WeaponCard(id, "Kanabo", CardType.WEAPON, 2, 3);
		} else if (id == 30) {
			c1 = new WeaponCard(id, "Tanegashima", CardType.WEAPON, 1, 5);
		} else if (id == 31) {
			c1 = new WeaponCard(id, "Nodachi", CardType.WEAPON, 3, 3);
		} else if (id >= 32 && id <= 37) {
			c1 = new PropertyCard(id, "Concentration", CardType.PROPERTY, "Can use extra weapon in same turn");
		} else if (id >= 38 && id <= 39) {
			c1 = new PropertyCard(id, "Bushido", CardType.PROPERTY,
					"Play against any player, In you turn get a card from the stack if it's a weapon waste one of you weapons or lose 1 Honor and waste this card, otherwise waste this card and pass Bushido to next player");
		} else if (id >= 40 && id <= 42) {
			c1 = new PropertyCard(id, "Fast unsheathed", CardType.PROPERTY, "You weapons does 1 extra damage");
		} else if (id >= 43 && id <= 46) {
			c1 = new PropertyCard(id, "Armor", CardType.PROPERTY, "You have a extra Dificulty when some atack you");
		} else if (id >= 47 && id <= 51) {
			c1 = new ActionCard(id, "Distraction", CardType.ACTION, "Get a card from the Stack");
		} else if (id >= 52 && id <= 54) {
			c1 = new ActionCard(id, "Daimio", CardType.ACTION,
					"Get 2 cards from the Stack, if you have this card in you hand at the end you get 1 honor extra");
		} else if (id >= 55 && id <= 57) {
			c1 = new ActionCard(id, "Breathing", CardType.ACTION,
					"Restore all you Resistence points and select 1 player to get card from the stack");
		} else if (id >= 58 && id <= 63) {
			c1 = new ActionCard(id, "Geisha", CardType.ACTION, "Waste a card used in the game or other players hand");
		} else if (id >= 64 && id <= 66) {
			c1 = new ActionCard(id, "Jiu-jitsu", CardType.ACTION,
					"Rest of the players must chose lose 1 weapon or 1 Resistence");
		} else if (id >= 67 && id <= 70) {
			c1 = new ActionCard(id, "Battle Cry", CardType.ACTION,
					"Rest of the users most select between lose 1 Resistence or use Stop Card");
		} else if (id >= 71 && id <= 74) {
			c1 = new ActionCard(id, "Tea ceremony", CardType.ACTION,
					"Get 3 cards from the stack, rest of the players gets 1 card from the stack");
		} else if (id >= 75 && id <= 89) {
			c1 = new ActionCard(id, "Stop", CardType.ACTION, "Blocks any attack");
		}
		return c1;
	}

	@Override
	public boolean equals(Object obj) {
		boolean ok = false;
		if (obj instanceof Card) {
			ok = this.cardID == ((Card) obj).getCardID();
		}
		return ok;
	}

}
