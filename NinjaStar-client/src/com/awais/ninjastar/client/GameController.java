/* GameController.java 1.0
 *
 *	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.awais.ninjastar.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import com.awais.ninjastar.hand.Card;
import com.awais.ninjastar.message.CardUsageMessage;
import com.awais.ninjastar.message.ErrorMessage;
import com.awais.ninjastar.message.HandshakeMessage;
import com.awais.ninjastar.message.LoadGameMessage;
import com.awais.ninjastar.message.Message;
import com.awais.ninjastar.message.MessageFactory;
import com.awais.ninjastar.message.OptionsMessage;
import com.awais.ninjastar.message.PlayerUpdateMessage;
import com.awais.ninjastar.message.TurnEndMessage;
import com.awais.ninjastar.player.CharactersRolesUtility;
import com.awais.ninjastar.player.Player;
import com.awais.ninjastar.player.PublicProfile;

/**
 * The controller for the GameActivity, controls all the flow of the game in the
 * client side
 * 
 * @author Awais Iqbal Begum
 */
public class GameController implements Observer {

	/**
	 * The client instance
	 */
	private Client mClient;

	/**
	 * All the players in the game
	 */
	private ArrayList<Player> players = new ArrayList<>();

	/**
	 * Own player's seat.
	 */
	private int ownPlayerSeat;

	/**
	 * Scanner used to read keyboard
	 */
	private Scanner sc;

	/**
	 * Boolean who is True if the game is started
	 */
	private boolean isStarted = false;

	/**
	 * Main Constructor of the GameController
	 */
	public GameController() {

		// get the instance of the client
		this.mClient = new Client();

		// add this class like a observer in the client
		this.mClient.addObserver(this);

		sc = new Scanner(System.in);

		// start a new thread to run the game
		new Thread() {
			public void run() {
				mClient.joinGame();
			}
		}.start();

	}

	/**
	 * This method send a message to the server to do a broadcast
	 * 
	 * @param msg
	 *            {@link Message} to send to server
	 */
	public void sendMsgToServer(Message msg) {
		// validate the message is different then null
		if (msg != null) {
			// send the message to client to send to the server without callback
			mClient.sendMsg(msg, null);

		}
	}

	/**
	 * Receives the messages/actions from the players through the peer
	 * connection and executes them.
	 */
	@Override
	public void update(Observable observableObject, final Object arg1) {

		// get the changed message throw the parameter
		final Message message = (Message) arg1;
		if (message == null) {
			System.err.println("Received message is Null");
			return;
		}

		// call the method onMessageRecieved to do actions depending in
		// the received codes
		onMessageRecieved(message);

	}

	/**
	 * This method is called when the used is going to disconnect from the game
	 */
	public void disconnect() {

		// callback for when the server receive the disconnect
		Runnable callback = new Runnable() {
			@Override
			public void run() {
				// Disconnect the client from the server
				System.out.println("Disconecting from server...");
				mClient.onServerDisconnect();
			}

		};

		// create a disconnect message and send it to the client to warn all the
		// connected player to disconnect im from the game
		mClient.sendMsg(MessageFactory.createDisconnectMessage(), callback);

	}

	/**
	 * This method is called when the player receive a message
	 * 
	 * @param message
	 *            {@link Message} received from the server
	 */
	public void onMessageRecieved(Message message) {
		int pid = message.getPlayerId();
		switch (message.getCode()) {
		case MessageFactory.MSG_DISCONNECT:
			System.out.println("The server has disconected!");
			break;
		case MessageFactory.MSG_GAMEEND:
			System.out.println("The game has ended...");
			isStarted = false;
			break;
		case MessageFactory.MSG_GAMESTART:
			System.out.println("The game has started!");
			isStarted = true;
			break;
		case MessageFactory.MSG_HANDSHAKE:
			HandshakeMessage Hsmessage = (HandshakeMessage) message;
			System.out.println("We have the seat number: " + Hsmessage.getOwnPlayerSeat());
			this.setOwnPlayerSeat(Hsmessage.getOwnPlayerSeat());
			break;
		case MessageFactory.MSG_LOADGAME:
			System.out.println("Loading players information...");
			System.out.println(message.toJson());
			LoadGameMessage lgm = (LoadGameMessage) message;
			Player p = null;
			for (int i = 0; i < lgm.getNames().size(); i++) {
				p = new Player();
				PublicProfile pp = new PublicProfile();
				pp.setName(lgm.getNames().get(i));
				p.setProfile(pp);
				p.setRole(CharactersRolesUtility.CreateRole(lgm.getRoles().get(i)));
				p.setPj(CharactersRolesUtility.CreateCharacters(lgm.getCharacters().get(i)));
				List<Card> lc = new ArrayList<>();
				for (int j = 0; j < lgm.getHands().get(i).size(); j++) {
					lc.add(Card.getByID(lgm.getHands().get(i).get(j)));
				}
				p.setHand(lc);
				p.setArmor(lgm.getArmors().get(i));
				p.setExtraDificulty(lgm.getExtraDificultys().get(i));
				p.setHonor(lgm.getHonors().get(i));
				p.setResistence(lgm.getResistences().get(i));
				p.setSeat(i);
				players.add(p);
			}

			System.out.println("Players information loaded!");
			break;
		case MessageFactory.MSG_NULL:
			System.err.println("Null received");
			break;
		case MessageFactory.MSG_TURNEND:
			System.out.println("Player at seat " + pid + " has ended hes turn");
			break;
		case MessageFactory.MSG_TURNSTART:
			System.out.println("Player at seat:  " + pid + " starts the turn");
			if (pid == this.ownPlayerSeat) {
				System.out.println("It's my turn!!");
				takeActions();
			}
			break;
		case MessageFactory.MSG_PLAYERUPDATE:
			System.out.println("Player " + pid + " information is updated");
			PlayerUpdateMessage pum = (PlayerUpdateMessage) message;
			updatePlayerInfo(pum);
			printPlayersInfo();
			break;
		case MessageFactory.MSG_ERRORMESSAGE:
			System.err.println("Received a error message:");
			manageError(message);
			break;
		case MessageFactory.MSG_OPTIONS:
			System.out.println("Receiving options...");
			OptionsMessage om = (OptionsMessage) message;
			gestionarOpciones(om);
		default:
			System.err.println("Unknow message received!");
			break;
		}
	}

	/**
	 * This function manage all the options received by the server
	 * 
	 * @param om
	 *            {@link OptionsMessage} received
	 */
	private void gestionarOpciones(OptionsMessage om) {
		for (int i = 0; i < om.getOptions().size(); i++) {
			System.out.println(i + ": " + om.getOptions().get(i));
		}
		int op = sc.nextInt();
		om.setSelectedOption(op);
		sendMsgToServer(om);
	}

	/**
	 * Prints the message received from the server
	 * 
	 * @param message
	 *            Message received from the server
	 */
	private void manageError(Message message) {
		ErrorMessage em = (ErrorMessage) message;
		System.err.println(em.getErrorMessage());
	}

	/**
	 * Interactive UI to get actions from the player
	 */
	private void takeActions() {
		int op = 0;
		Message msg = null;
		if (isStarted) { // if the game is started
			printMenuAcciones();// print all the posible actions
			op = sc.nextInt(); // get the action from the user
			if (op == 1) {// case when user select use Card
				printAllUsableCards();
				System.out.println("Indicate the ID of the card to use:");
				int id = sc.nextInt();
				int objective = -1;
				if (hasObjectivePlayer(id)) { // check if that card has a
												// objective
					System.out.println("Indicate you objective (diferent to " + ownPlayerSeat + " )");
					objective = sc.nextInt();
					msg = MessageFactory.createCardUsageWithObjectivePlayer(id, objective);
				} else if (hasObjectiveCard(id)) { // check if that card has a
													// objective
					System.out.println("Indicate objective cardID:");
					objective = sc.nextInt();
					msg = MessageFactory.createCardUsageWithObjectiveCard(id, objective);
				} else {
					msg = MessageFactory.createCardUsage(id);
				}

			} else if (op == -1) { // End turn
				sendTurnEnd();
				msg = MessageFactory.createTurnEndMessage(ownPlayerSeat);
			}
			sendMsgToServer(msg);
		}
	}

	/**
	 * Given a cardID check if that card can have a objective player
	 * 
	 * @param id
	 *            Used card ID
	 * @return True if that can have objective
	 */
	private boolean hasObjectivePlayer(int id) {
		return ((id >= 0 && id <= 31) || (id == 38 || id == 39));
	}

	/**
	 * Given a cardID check if that card can have a objective player
	 * 
	 * @param id
	 *            Used card ID
	 * @return True if that can have objective
	 */
	private boolean hasObjectiveCard(int id) {
		return (id >= 58 && id <= 63);
	}

	/**
	 * Sends a {@link TurnEndMessage} to the server to finish our turn
	 */
	private void sendTurnEnd() {
		TurnEndMessage tem = MessageFactory.createTurnEndMessage(ownPlayerSeat);
		sendMsgToServer(tem);
	}

	/**
	 * Prints all the cards of our player
	 */
	@SuppressWarnings("unused")
	private void printAllCards() {
		Player p = players.get(ownPlayerSeat);
		for (int i = 0; i < p.getHand().size(); i++) {
			System.out.println(p.getHand().get(i).toString());
		}
	}

	/**
	 * Prints all the cards of our player
	 */
	private void printAllUsableCards() {
		Player p = players.get(ownPlayerSeat);
		System.err.println("current cardsSIze: " + p.getHand().size());
		for (int i = 0; i < p.getHand().size(); i++) {
			Card c = p.getHand().get(i);
			System.err.println(i);
			if (c.getCardID() > 89 || c.getCardID() < 75)
				System.out.println(c.toString());
		}
	}

	/**
	 * Prints all the possible actions
	 */
	private void printMenuAcciones() {
		System.out.println("********************");
		System.out.println("1.   Use a  card    ");
		System.out.println("-1.   End turn      ");
		System.out.println("********************");

	}

	/**
	 * Given a {@link PlayerUpdateMessage} updates the information of that
	 * player
	 * 
	 * @param pum
	 *            {@link PlayerUpdateMessage} with all the new information of
	 *            the player
	 */
	private void updatePlayerInfo(PlayerUpdateMessage pum) {
		Player p = players.get(pum.getPlayerId());
		// TODO update only the information which has changed
		Boolean of = pum.getOfensive();
		if (of != null)
			p.setOfensive(of);
		Integer tmp = pum.getArmor();
		if (tmp != null)
			p.setArmor(tmp);
		tmp = pum.getExtraDificulty();
		if (tmp != null)
			p.setExtraDificulty(tmp);
		tmp = pum.getHonor();
		if (tmp != null)
			p.setHonor(tmp);
		tmp = pum.getResistence();
		if (tmp != null)
			p.setResistence(tmp);
		List<Integer> al = pum.getHand();
		if (al != null) {
			List<Card> hand = getCardsFromID(al);
			p.setHand(hand);

		}

	}

	/**
	 * Given a list of IDs returns a list of cards with that ID
	 * 
	 * @param al
	 *            List with all the IDs
	 * @return List with {@link Card} getted by that ID
	 */
	private List<Card> getCardsFromID(List<Integer> al) {
		List<Card> ac = new ArrayList<>();
		for (int i = 0; i < al.size(); i++) {
			ac.add(Card.getByID(al.get(i)));
		}
		return ac;
	}

	/**
	 * Prints all the players seated in the table information
	 */
	private void printPlayersInfo() {
		Player p;
		for (int i = 0; i < players.size(); i++) {
			p = players.get(i);
			System.out.println(
					"Seat: " + p.getSeat() + " | Honor: " + p.getHonor() + " | Resistence: " + p.getResistence()
							+ " | ExtraDif: " + p.getExtraDificulty() + " | Cards left: " + p.getHand().size());
		}
	}

	/**
	 * This method return a PublicProfile seated in the X table position
	 * 
	 * @param seatPos
	 *            The position of the player in the table
	 * @return PublicProfile of the player seated in that position
	 */
	public PublicProfile getPlayerProfile(int seatPos) {
		// TODO pending to implement a public profile for each user
		return null;
	}

	// ------------ GETTERS AND SETTERS ------------

	public int getOwnPlayerSeat() {
		return ownPlayerSeat;
	}

	public void setOwnPlayerSeat(int ownPlayerSeat) {
		this.ownPlayerSeat = ownPlayerSeat;
	}

	public static void main(String[] args) {
		new GameController();
	}
}
