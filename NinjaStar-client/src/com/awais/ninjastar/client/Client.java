/* Client.java 1.0
 *
 *	 Copyright 2017	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.awais.ninjastar.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.awais.ninjastar.message.Message;
import com.awais.ninjastar.message.MessageFactory;

/**
 * The Client class to connect to server to get information, mostly used by
 * {@link GameActivity}
 * 
 * @author Awais Iqbal Begum
 */
public class Client extends Observable {

	/**
	 * This field is used by the Log to show the class name
	 */
	private static final String TAG = "Client";

	/**
	 * Server host where to connect
	 */
	private static String SERVER_HOST = "localhost";

	/**
	 * Port where the client will connect
	 */
	private static int SERVER_PORT = 5253;

	/**
	 * how client Socket
	 */
	private Socket socket;

	/**
	 * The output used by Client
	 */
	private PrintWriter out;

	/**
	 * The input used by the CLient
	 */
	private BufferedReader in;

	/**
	 * The reading Thread
	 */
	private ReadThread readThread;

	/**
	 * A Thread pool used to write messages to other peers without blocking
	 * either the UI Thread or the readThread from the peers.
	 */
	private WriteThreadPoolAdapter writeThreadPool;

	/**
	 * Default contructor
	 */

	public Client() {
		writeThreadPool = new WriteThreadPoolAdapter();
	}

	/**
	 * This Method join a existing game
	 */
	public void joinGame() {
		// initialize the socket
		initSocket();
	}

	/**
	 * This method connect the socket to the server
	 */
	public void initSocket() {
		if (socket != null) {
			return;
		}
		try {
			// create the socket using the server host and the port
			socket = new Socket(SERVER_HOST, SERVER_PORT);

			// get the output from the server
			out = new PrintWriter(socket.getOutputStream(), true);

			// create a input reader from the server
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// create the reader thread and start it
			readThread = new ReadThread();
			readThread.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method send a message to the server
	 * 
	 * @param msg
	 *            {@link Message} to send to server
	 */
	public void sendMsg(final Message msg, Runnable callback) {
		// check if the message is diferent then null
		if (msg == null) {
			return;
		}
		// send the message to server
		sendMsg(msg.toJson(), callback);
		System.err.println("mensaje entregado");
	}

	/**
	 * This method send a message to the server
	 * 
	 * @param msg
	 *            Get a String message to send to server
	 * 
	 */
	public void sendMsg(final String msg, final Runnable callback) {
		// create a new runnable to send message
		Runnable cmd = new Runnable() {
			@Override
			public void run() {
				// when the thread is running print the message
				out.println(msg);
				System.out.println("CLIENT: message Send!");
				if (callback != null) {
					// if there are any callback to do run the callback
					callback.run();
				}
			}
		};
		// Execute the command in the pool.
		writeThreadPool.write(cmd);
	}

	/**
	 * This method is called when the user will disconnect
	 */
	public void onServerDisconnect() {
		try {
			readThread.end = true;
			if (!socket.isClosed()) {
				socket.getInputStream().close();
				socket.close();
				System.out.println(TAG + ": " + socket.isClosed() + " is closed");
				System.exit(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * A Class that represents a Thread listening to the messages sent by this
	 * PeerConnection socket.
	 * 
	 * It listens to new messages until end() is called. Everytime a message is
	 * recieved the Peer's onMessageRecieved event will be raised.
	 * 
	 */
	private class ReadThread extends Thread {

		/**
		 * A flag indicating whether this thread should keep listening
		 */
		private boolean end = false;

		public void run() {

			// While an end request hasn't been requested.
			while (!end) {

				try {
					// Messages are sent by json with an end of line at the end.
					// The json messages sent cannot have new lines in the
					// middle.
					String input = in.readLine();

					Message msg = MessageFactory.create(input);

					Client.this.setChanged(); // Notify to gamecontroller that
												// we received a new Message
					Client.this.notifyObservers(msg);

					if (input == null) {
						return;
					}
				} catch (IOException e) {
					System.out.println("Closing conexion...");
					onServerDisconnect();
				}

			}
		}

		/**
		 * Kills the thread.
		 */
		@SuppressWarnings("unused")
		public void kill() {
			end = true;
		}
	}

	/**
	 * A class that follows the Adapter pattern to adapt the ThreadPoolExecutor
	 * to the Peer class. It has a write method that executes the given message
	 * on a thread.
	 * 
	 */
	private static class WriteThreadPoolAdapter {

		/**
		 * The adapter. This pool will be used to send the messages to the
		 * connected peers.
		 */
		private ThreadPoolExecutor pool;

		/**
		 * Default options of the pool.
		 */
		static final int defaultCorePoolSize = 5;

		static final int defaultMaximumPoolSize = 10;

		static final long defaultKeepAliveTime = 600;

		final TimeUnit defaultTimeUnit = TimeUnit.SECONDS;

		final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();

		public WriteThreadPoolAdapter() {
			pool = new ThreadPoolExecutor(defaultCorePoolSize, defaultMaximumPoolSize, defaultKeepAliveTime,
					defaultTimeUnit, workQueue);
		}

		public void write(Runnable cmd) {
			pool.execute(cmd);
		}
	}

}
